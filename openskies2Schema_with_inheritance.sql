-- destroy all existing tables
-- N.B. destroy "many" before "one" e.g. transactions before accounts
DROP TABLE IF EXISTS bank_transactions;
DROP TABLE IF EXISTS bank_accounts;

-- now (re)create them
CREATE TABLE bank_accounts 
     ( account_no integer not null AUTO_INCREMENT,
       account_name varchar(30) not null,
       balance decimal(12,2) not null,	     
       overdraft_limit decimal(12,2) null, -- only current a/c's
       max_number_trans integer null,      -- only deposit a/c's
       account_type varchar(2)             -- the DISCRIMINATOR column
       PRIMARY KEY(account_no)
     )ENGINE=INNODB;

CREATE TABLE bank_transactions 
     ( trans_no integer not null AUTO_INCREMENT,
       trans_date date not null,
       trans_amount decimal(12,2) null,	     
       debit_credit char(1) not null,
       account_no integer not null,
       PRIMARY KEY(trans_no),
       FOREIGN KEY(account_no) REFERENCES bank_accounts (account_no)
     )ENGINE=INNODB;

