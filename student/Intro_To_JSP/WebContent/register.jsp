<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>

<%--
		QA To do: Exercice 1 step 7
		
		Add a JSP page directive to import the Java
		package java.util.
--%>

</head>

<BODY>

<DIV align="center">
<H1>Festival of Culture</H1>
</DIV>

<DIV align="left">
<H2>Please type in your details:</H2>

<%--
		QA To do: Exercise 1 step 5
		
		If you have aliased your JSP, make the action
		of the form use the alias.
--%>

<FORM method="get" action="viewdetails.jsp">
<TABLE border="0">
<TR>
<TD width="100" align="right"><B>Name: </B></TD>
<TD><INPUT type="text" name="fullname"></TD>
</TR>
<TR>
<TD width="100" align="right"><B>Age: </B></TD>
<TD><INPUT type="text" name="age"></TD>
</TR>
<TR>
<TD width="100" align="right"><B>Email: </B></TD>
<TD><INPUT type="text" name="email"></TD>
</TR>
</TABLE>
<BR><BR>
</DIV>
<DIV align="left">
<H2>What types of events are you interested in?</H2>

<TABLE border="0">
<TR>
<TD width="100" align="right"><INPUT type="checkbox" name="interest" value="opera"></TD>
<TD align="left"><B>Opera</B></TD>
</TR>
<TR>
<TD width="100" align="right"><INPUT type="checkbox" name="interest" value="ballet"></TD>
<TD align="left"><B>Ballet</B></TD>
</TR>
<TR>
<TD width="100" align="right"><INPUT type="checkbox" name="interest" value="concerts"></TD>
<TD align="left"><B>Classical Concerts</B></TD>
</TR>
<TR>
<TD width="100" align="right"><INPUT type="checkbox" name="interest" value="cinema"></TD>
<TD align="left"><B>Cinema</B></TD>
</TR>
<TR>
<TD width="100" align="right"><INPUT type="checkbox" name="interest" value="theatre"></TD>
<TD align="left"><B>Theatre</B></TD>
</TR>
</TABLE>
<BR><BR>
</DIV>
<DIV align="left">
<INPUT type="submit" value="REGISTER"></TD><TD></TD>
</FORM>
</DIV>

<BR><BR><HR><BR>

<%--
		QA To do: Exercise 1 step 7
		
		Use an initialisation parameter in a JSP
		expression to give e-mail contact details.
		Also print out the date when the page was
		processed.
--%>

<DIV>
Contact details: 
<A href="mailto:???">???</A>.
<BR>This page was processed at ???.
</DIV>

</body>
</html>
