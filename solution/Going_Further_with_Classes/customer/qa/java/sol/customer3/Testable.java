//
//  Testable.java
//
package qa.java.sol.customer3;
//
// This interface defines five no-argument "test" methods.
// It is designed for use with a test harness that use the
// LiveTable class.
//

public interface Testable
{
	void test1();
	void test2();
	void test3();
	void test4();
	void test5();
}
