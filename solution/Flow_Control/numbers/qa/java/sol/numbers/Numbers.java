/* Using loops */
package qa.java.sol.numbers;

public class Numbers
{
	public static void main(String argv[])
	{

		// Print out table of 2 to the power n where n < 64

		// First attempt using while loop
		/*
		int i = 1;
		long result = 1;
		while (i < 64)
		{
			result *= 2;
			if (result > 9999999999L)
				break;
			if (i < 10)
				System.out.print(" ");
			System.out.println(i + "\t" + result);
			i++;
		}

 		*/
		/* Second attempt using a for loop

		long result = 1;
		for (int i = 1; i < 64; i++)
		{
			result *= 2;
			if (result > 9999999999L)
				break;
			if (i < 10)
				System.out.print(" ");
			System.out.println(i + "\t" + result);
		}
		*/

		// Third (optional) attempt using a nested loop to
		// justify all numbers to the right

		
		for (long i = 1, result = 1; i < 64; i++)
		{
			result *= 2;
			if (result > 9999999999L)
				break;
	  		if (i < 10)
				System.out.print(" ");
			System.out.print(i + "\t");
			for (long j = 1; j <= 9999999999L; j *= 10)
	  		{
		  	 	if (result < j)
			  		System.out.print(" ");
			}
			System.out.println("" + result);
      		
 		}

    }

}

