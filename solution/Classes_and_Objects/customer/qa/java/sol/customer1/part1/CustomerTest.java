/*
 * CustomerTest class
 *
 * CustomerTest is a Java application for testing the Customer class.
 *
 */
package qa.java.sol.customer1.part1;

public class CustomerTest
{
	//
	// The main() method is the entry point of this class
	//
	public static void main(String[] args)
	{
		//
		// Declare two references to Customer objects
		//
		Customer customer1, customer2;

		//
		// Create two new customer objects
		//
	    customer1 = new Customer("Smith", 101);
        customer2 = new Customer("Jones", 102);

		// Display headings for Customers' details
		//
		System.out.println("\n" + "Customer" + "\t" + "A/C No." + "\n");

		// Display details of both Customers
		//

		System.out.println(customer1.name + "\t\t" + customer1.accountNumber);
		System.out.println(customer2.name + "\t\t" + customer2.accountNumber);

	}

}
