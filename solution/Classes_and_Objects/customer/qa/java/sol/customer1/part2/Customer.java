/* Class for customer */
package qa.java.sol.customer1.part2;

public class Customer
{
	// Variables to hold Customer's name and account number
	private String name;
    private int accountNumber;

	//
	// Constructor that takes a String and an int argument
	// to initialise the instance variables for the Customer's name
	// and account number, respectively.
	//
    public Customer(String n, int a)
    {
		name = n;
		accountNumber  = a;
	}


	//
	// The getName method returns the Customer's name
	//
    public String getName()
    {
		return name;
	}

	//
	// The getAccountNumber method returns the Customer's account number
	//
    public int getAccountNumber()
    {
		return accountNumber;
	}


	//
	// The setName method changes the Customer's name
	//
    public void setName(String n)
    {
		// Do a sanity check
		if (n != null && n.length() > 0) {
			name = n;
		}
	}


}
